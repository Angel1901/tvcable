<div class="col-lg-12 col-sm-12 col-12 formulario w-100 align-content-sm-center pb-3 pt-3">
	<div class="">   
		<div class="row m-0 col-12">
			<div class="row m-0 p-0">
			<div class="col-2 w-100 p-0 row align-items-center justify-content-center mr-2">
				<img class=" w-100" src="/img/icons/phone.png">	
			</div>
			<div class="col-10 p-0">
				<label class="RobotoBlack dejanosForm textBlue">¡Déjanos tu teléfono y te llamamos al instante!</label>
			</div>
		</div>
			<form id="formulario-principal" class="col-12 row m-0 p-0 form-callback">			
				<div class="col-12 row m-0 mb-2">
					<div class="col-12">
					<input type="tel" name="numero" class="form-control text-center inputForm" placeholder="Número de Teléfono" pattern="[09]{2}[0-9]{8}" title="El número de teléfono debe tener 10 dígitos" maxlength="10" required="required">
					<select class="col-12 mt-2 form-control text-center inputForm">
						<option>Triple Plack (Internet + Tel + TV)</option>
                        <option>Internet Residencial</option>
                        <option>Televisión por Cable</option>
                        <option>Televisión Satelital</option>
                        <option>Servicio al Cliente / Soporte Técnico / Facturación</option>
					</select>
					</div>
				</div>
				<div class="form-check check cronos m-2">
					<input class="form-check-input" type="checkbox" id="check_1" required="">
					<label class="form-check-label cAzul cronos" id="check_label_1" for="check_1">
						<label class="RobotoBlack terminosForm">Acepto Políticas de privacidad, términos y condiciones.</label>
					</label>
				</div>
				<div class="w-100 pt-3">
						
						<button class="btn btn-primary botonForm mt-0 mb-2 ml-auto mr-auto backgroundBlue4 borderButtom RobotoBlack pl-5 pr-5"><img class="pr-4" src="/img/icons/telefonoIcon.png">¡LLÁMAME AHORA!</button>
				</div>
			</form>  
		</div>
	</div>
</div>