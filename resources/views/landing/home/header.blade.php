<!-- Header -->
<header class="banner w-100 larosalia">
	<div class="col-12 backgroundBlue2 row m-0 m-sm-0 pt-3 pb-3 d-none d-sm-none d-md-none d-lg-block">
		<ul class="nav w-100 justify-content-center row m-0 ">
                    <li class="nav-item" role="presentation">
                    	<a class="textWhite Roboto menu p-2" href="/">
						<img class="logos" src="/img/icons/triple-pack-menu-1-8.png">TRIPLE PACK</a>
                    </li>
                    <li class="nav-item" role="presentation">
                    	<a class="textWhite Roboto menu p-2" href="#Internet">
						<img class="logos" src="/img/icons/internet-menu-2-8.png">INTERNET</a>
                    </li>
                    <li class="nav-item" role="presentation">
                    	<a class="textWhite Roboto menu p-2" href="#Television">
						<img class="logos" src="/img/icons/television-menu-3-8.png">TELEVISIÓN</a>	
                    </li>
                </ul>
	</div>
	<div class="text-center d-block d-md-block d-lg-block backgroundBlue3 pt-md-3">
		<div class="d-block d-md-block d-lg-none">
			<div class="w-100 pt-4">
				<span class="textWhite RobotoBlack mt-1 mb-1">LÍNEA EXCLUSIVA DE VENTAS</span>
			</div>
			<div class="w-100 text-center">
				<a class="btn btn-primary boton" href="#" role="button">
					<img class="faa-tada animated w-25" src="/img/icons/phone-icon-form.svg">Numero Te</a>
			</div>
			<div class="w-100 text-center">
				<p class="m-0 textWhite RobotoBlack mt-1 mb-1">Línea de Servicion al Cliente/Facturación</p>
				<p class="m-0 textWhite RobotoBlack mt-1 mb-1">Soporte Técnico: 600 4000</p>
			</div>
		</div>
		<div class="">
			<div class="card border rounded shadow form-box text-center">
				@include('landing.'.$modulo.'.form-principal')
			</div>
			<img class="w-100 d-none d-md-block d-lg-block" src="/img/imgs/1TargetersTRIPLEPACK.jpg">
			<img class="w-100 d-block d-md-none d-lg-none " src="/img/imgs/TargetersMOBILE-01.jpg">
		</div>
	</div>
</header>




                
                
            
            