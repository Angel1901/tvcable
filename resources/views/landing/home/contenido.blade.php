<section>
	<img class="w-100 d-none d-md-block d-lg-block" id="Promo" src="/img/imgs/4TargetersPROMODESC.jpg">
	<img class="w-100 d-block d-md-none d-lg-none" id="Promo" src="/img/imgs/TargetersMOBILE-03.jpg">
</section>
<section>
	<div class="card border rounded shadow form-box2 text-center">
		@include('landing.'.$modulo.'.form-principal2')
	</div>
	<img class="w-100 d-none d-md-block d-lg-block" id="Internet" src="/img/imgs/2TargetersINT.jpg">
	<img class="w-100 d-block d-md-none d-lg-none" id="Internet" src="/img/imgs/TargetersMOBILE-02.jpg">
</section>
<section>
	<div class="card border rounded shadow form-box3 text-center">
		@include('landing.'.$modulo.'.form-principal3')
	</div>
	<img class="w-100 d-none d-md-block d-lg-block" id="Television" src="/img/imgs/3TargetersTV.jpg">
	<img class="w-100 d-block d-md-none d-lg-none" id="Television" src="/img/imgs/TargetersMOBILE-05.jpg">
</section>




