<div class="barraSuperior w-100  ">
  <div class="text-center d-sm-none d-sm-none d-md-none d-lg-block">
    <span class="targeters RobotoBlack">TARGETERS DISTRIBUIDOR AUTORIZADO</span>  
  </div>
</div>
<div class="d-block d-sm-block d-md-block d-lg-block ">
  <div class="col-12 w-100 row m-0 nav backgroundBlue posNav align-items-center p-0 d-none d-sm-none d-md-none d-lg-block">
      <div class="row m-0 w-100">
        <div class="col-5 nav-item justify-content-center p-0 logonav">
          <a class="nav-link d-none d-sm-none d-md-block d-lg-block p-0" href="/">
            <img class="w-100" src="/img/imgs/LANDING_TVcableBannerWEB.png" alt=""> </a>
        </div>
        <div class="col-7 row justify-content-end m-0 w-100 p-0 ml-auto">
          <div class="col-12 row m-0 p-0">
            <div class="col-4 row align-items-center p-0">
              <span class="w-100 textWhite RobotoBlack">LÍNEA EXCLUSIVA DE VENTAS</span>
            </div>
            <div class="col-4 row align-items-center justify-content-center p-0">
              <a class="btn btn-primary boton" href="#" role="button"><img class=" faa-tada animated w-25" src="/img/icons/phone-icon-form.svg">Numero Te</a>
            </div>
            <div class="col-4 text-center RobotoBlack atencionalcliente">
              <p class="m-0 w-100 textWhite RobotoBlack">Línea de Servicion al Cliente / Facturación</p>
              <p class="m-0 w-100 textWhite RobotoBlack">Soporte Técnico: 600 4000</p>
            </div>  
        </div>
      </div>
  </div>  
</div>    
<nav class="w-100 navbar navbar-expand-lg navbar-light backgroundBlue2 bg-light d-block d-sm-block d-md-block d-lg-none posNav">
  <div class="row m-0">
  <div class="col-9 col-md-4 p-0 w-100">
    <a class="mr-5" href="#"><img class="w-100 logonav" src="/img/imgs/LANDING_TVcableMovil.png"></a>  
  </div>
  <div class="col-3 justify-content-end p-0 row align-items-center d-block d-sm-block d-md-none d-lg-none">
    <button class="ml-2 navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="fas fa-bars"></span>
  </button>
  </div>
  <div class="row m-0 col-md-8 d-none d-sm-none d-md-block d-lg-none">
    <div class="">
     <label class="mb-2 w-auto">
        <a class="textWhite Roboto menu p-2" href="/">
        <img class="logos " src="/img/icons/triple-pack-menu-1-8.png">TRIPLE PACK</a>
      </label>
      <label class="nav-item mt-2 mb-2 w-auto" role="presentation">
        <a class="textWhite Roboto menu p-2" href="#Internet">
        <img class="logos " src="/img/icons/internet-menu-2-8.png">INTERNET</a>
      </label>
      <label class="nav-item mt-2 mb-2 w-auto" role="presentation">
        <a class="textWhite Roboto menu p-2" href="#Television">
        <img class="logos " src="/img/icons/television-menu-3-8.png">TELEVISIÓN</a>
      </label>
    </div> 
  </div>
  </div>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
     <li class="mb-2">
        <a class="textWhite Roboto menu p-2" href="/">
        <img class="logos " src="/img/icons/triple-pack-menu-1-8.png">TRIPLE PACK</a>
      </li>
      <li class="nav-item mt-2 mb-2" role="presentation">
        <a class="textWhite Roboto menu p-2" href="#Internet">
        <img class="logos " src="/img/icons/internet-menu-2-8.png">INTERNET</a>
      </li>
      <li class="nav-item mt-2 mb-2" role="presentation">
        <a class="textWhite Roboto menu p-2" href="#Television">
        <img class="logos " src="/img/icons/television-menu-3-8.png">TELEVISIÓN</a>
      </li>
    </ul>
  </div>
</nav>
