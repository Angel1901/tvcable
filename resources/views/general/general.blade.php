<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>ETB</title>

        <style type="text/css">
            .loader {
                position: fixed;
                left: 0px;
                top: 0px;
                width: 100%;
                height: 100%;
                z-index: 9999;
                background: url('img/pageLoader.gif') 50% 50% no-repeat rgb(249,249,249);
                opacity: 1;
            }
        </style>
    </head>
    <body>
        <div class="loader"></div>
        @yield('nav')
        @yield('header')
        @yield('content')
        @yield('footer')
        <input type="hidden" id="_token" value="{{ csrf_token() }}">
    </body>

    <script type="text/javascript" src="{{asset('librerias/jquery/jquery.min.js')}}"></script>
    <script type="text/javascript">    
        var listaNegra=@json($lista_negra);
        var horario_atencion=@json($horario_atencion);
        var lista_tsource=@json($tsourse);

        function downloadJSAtOnload() {
            setTimeout(function() {
                $(".loader").css("display","none");
                // script vaalidacion
                var element = document.createElement("script");
                element.src = "librerias/jquery/jquery.validate.js";
                document.body.appendChild(element);
                // script general
                var element = document.createElement("script");
                element.src = "js/general.js";
                document.body.appendChild(element);

                // script modulo
                var js= @json($js);
                for (var i = js.length - 1; i >= 0; i--) {
                    var element = document.createElement("script");
                    element.src = js[i];
                    document.body.appendChild(element);
                }
            }, 200);
            // script bootstrap
            var element = document.createElement("script");
            element.src = "librerias/bootstrap/js/bootstrap.js";
            document.body.appendChild(element);
            // script bootstrap min
            var element = document.createElement("script");
            element.src = "librerias/bootstrap/js/bootstrap.min.js";
            document.body.appendChild(element);
            // script fontweasome
            var element = document.createElement("script");
            element.src = "librerias/fontawesome/js/all.min.js";
            document.body.appendChild(element);
            
            // script callback
            var element = document.createElement("script");
            element.src = "js/callback.js";
            document.body.appendChild(element);
            // script validacion
            var element = document.createElement("script");
            element.src = "js/validacion.js";
            document.body.appendChild(element);
        }
        
        if (window.addEventListener){
            window.addEventListener("load", downloadJSAtOnload, false);
        }else if (window.attachEvent){
            window.attachEvent("onload", downloadJSAtOnload);
        }else{ 
            window.onload = downloadJSAtOnload;
        }

        function downloadCSSAtOnload() {
            var style = document.createElement("link");
            style.async = false;
            style.rel="stylesheet"; 
            style.type="text/css";
            style.href = 'librerias/bootstrap/css/bootstrap.css';
            document.head.appendChild(style);

            var style = document.createElement("link");
            style.async = false;
            style.rel="stylesheet"; 
            style.type="text/css";
            style.href = 'librerias/bootstrap/css/bootstrap.min.css';
            document.head.appendChild(style);

            var style = document.createElement("link");
            style.async = false;
            style.rel="stylesheet"; 
            style.type="text/css";
            style.href = 'librerias/fontawesome/css/all.min.css';
            document.head.appendChild(style);

            var style = document.createElement("link");
            style.async = false;
            style.rel="stylesheet"; 
            style.type="text/css";
            style.href = 'css/general.css';
            document.head.appendChild(style);

            var css= @json($css);
            for (var i = css.length - 1; i >= 0; i--) {
                var style = document.createElement("link");
                style.async = false;
                style.rel="stylesheet"; 
                style.type="text/css";
                style.href = css[i];
                document.head.appendChild(style);
            }
        }

        if (window.addEventListener){
            window.addEventListener("load", downloadCSSAtOnload, false);
        }else if (window.attachEvent){
            window.attachEvent("onload", downloadCSSAtOnload);
        }else{ 
            window.onload = downloadCSSAtOnload;
        }
    </script>
</html>