<?php
return [
	"fijo"=>[
				"default"=>["nombre"=> "tel:", "default"=>1, "numero"=> "0314437852", "lbl_numero"=>"0314437852" ]
			],

	"movil"=>[
				"default"=>[""=> "nombre", "default"=>1, "numero"=> "0314437770", "lbl_numero"=>"314437770" ],
				"numero"=>["nombre"=> "Tel: ", "default"=>0, "numero"=> "0314325982","lbl_numero"=>"0314325982" ],
				"telefono"=>["nombre"=> "Tel:", "default"=>0, "numero"=> "0314434121","lbl_numero"=>"0314434121" ],
				"oferta-fusagasuga"=>["nombre"=> "fusagasuga", "default"=>0, "numero"=> "0314437771","lbl_numero"=>"0314437771" ],
				"oferta-bogota"=>["nombre"=> "Bogotá", "default"=>0, "numero"=> "0314437851","lbl_numero"=>"0314437851" ],
				"oferta-cantalejo"=>["nombre"=> "Cantalejo", "default"=>0, "numero"=> "0314437771","lbl_numero"=>"0314437771" ],
				"oferta-soacha"=>["nombre"=> "Soacha", "default"=>0, "numero"=> "0314437771","lbl_numero"=>"0314437771" ],
				"cajica"=>["nombre"=> "Cajica", "default"=>0, "numero"=> "0314437773","lbl_numero"=>"0314437773" ],
				"chia"=>["nombre"=> "Chia", "default"=>0, "numero"=> "0314437773","lbl_numero"=>"0314437773" ],
				"oferta-display"=>["nombre"=> "Tel:", "default"=>0, "numero"=> "0314437774","lbl_numero"=>"0314437774" ],
				"oferta-mail"=>["nombre"=> "Tel:", "default"=>0, "numero"=> "0314437776","lbl_numero"=>"0314437776" ],
				"bing-oferta"=>["nombre"=> "Tel:", "default"=>0, "numero"=> "0314437777","lbl_numero"=>"0314437777" ],
				"medellin"=>["nombre"=> "Medellin", "default"=>0, "numero"=> "0314437779","lbl_numero"=>"0314437779" ],
				"antioquia"=>["nombre"=> "Antioquia", "default"=>0, "numero"=> "0314437779","lbl_numero"=>"0314437779" ],
				"ventas"=>["nombre"=> "Ventas", "default"=>0, "numero"=> "0314437850","lbl_numero"=>"0314437850" ],
				"oferta-sms"=>["nombre"=> "Tel:", "default"=>0, "numero"=> "0314437858","lbl_numero"=>"0314437858" ],
				"oferta-programatic"=>["nombre"=> "Tel", "default"=>0, "numero"=> "0314437859","lbl_numero"=>"0314437859" ],
				"portabilidad"=>["nombre"=> "Tel:", "default"=>0, "numero"=> "0317562523","lbl_numero"=>"0317562523" ]
	],
	"movil2"=>[
		"default"=>["nombre"=> "tel:", "default"=>1, "numero"=> "0314437852", "lbl_numero"=>"0314437852" ],
		"display"=>["nombre"=> "tel:", "default"=>0, "numero"=> "0314437853","lbl_numero"=>"0314437853" ],
		"sms"=>["nombre"=> "tel:", "default"=>0, "numero"=> "0314437854","lbl_numero"=>"0314437854" ],
		"mailing"=>["nombre"=> "tel:", "default"=>0, "numero"=> "0314437855","lbl_numero"=>"0314437855" ],
		"bing"=>["nombre"=> "tel:", "default"=>0, "numero"=> "0314437856","lbl_numero"=>"0314437856" ]
	]
];
?>